# Google Music To Spotify Playlist converter

- Use Chrome and go to a Google Music playlist such as:

https://play.google.com/music/preview/pl/AMaBXykwt-AayWgllMtusoH13LX6fbi1UTghqPUoc9jTbEqthKOfOUEA3EPkaNjq0IlnTz4chHHqfU2UO9Tz9CdnzRqpHKSbSQ==?u=0

https://play.google.com/music/preview/pl/AMaBXynsuj_J10Rt97yYOsnxEgATApP9mkenunsIgaqWKcjSuFAQjP5zsTXdJOEyMkcRAJe2JSbGn9DSSgsk9qYzpN1fStEPqA==?u=0

- Open the Developer console and paste in the contents of [export.js](export.js)

- Copy the output and paste into Ms Excel 

- Use http://www.playlist-converter.net to convert and import into your Spotify account

